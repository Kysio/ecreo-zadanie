// Gulp

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');

/******************************/

// ENV

const ENV = process.env.NODE_ENV || 'development';
const VALID_ENV_LIST = ['development', 'production'];

if (VALID_ENV_LIST.indexOf(ENV) === -1) {
    throw new Error('Invalid ENV '+ ENV +' use '+ JSON.stringify(VALID_ENV_LIST));
}

/******************************/

// SASS

function taskSass() {
    return gulp.src('src/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('static/'+ ENV +'/css'));
}

// html

function taskHtml() {
    return gulp.src('src/html/*.html')
        .pipe(gulp.dest('static/'+ ENV));
}

// copy

function taskCopyAssets() {
    return Promise.all([
        gulp.src('assets/**/*')
            .pipe(gulp.dest('static/common')),
        gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
            .pipe(gulp.dest('static/common/fonts/webfonts'))
    ]);
}

// watch

function taskWatch(done) {
    gulp.watch('src/sass/**/*', taskSass);
    gulp.watch('src/html/*', taskHtml);
    gulp.watch('assets/**/*', taskCopyAssets);
    done();
}

// default

let mainTask = gulp.series(
    gulp.parallel(taskSass, taskHtml, taskCopyAssets)
);

// develop

let developTask = gulp.series(
    mainTask,
    taskWatch,
    function syncBrowser() {
        let bs = browserSync.create();

        bs.init({
            server: {
                baseDir: `./static/${ ENV }`,
                index: 'index.html'
            },
            serveStatic: [
                `./static/${ ENV }`,
                `./static/common`
            ]
        });

        gulp.watch(`static/${ ENV }/*.html`).on('change', bs.reload);
        gulp.watch(`static/${ ENV }/css/*.css`).on('change', bs.reload);
    }
);

Object.assign(exports, {
    default: mainTask,
    html: taskHtml,
    sass: taskSass,
    watch: taskWatch,
    develop: developTask
});
